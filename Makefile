test:
	pytest --cov=flinter

lint:
	pylint flinter

doc:
	cd docs && make html

wheel:
	rm -rf build
	rm -rf dist
	python setup.py sdist bdist_wheel

upload_test:
	twine upload -u COOPTeam-CERFACS --repository-url https://test.pypi.org/legacy/ dist/* 

upload:
	twine upload -u COOPTeam-CERFACS dist/*

gitlab_git_origin:
	git remote set-url origin https://gitlab.com/cerfacs/flint.git

nitrox_git_origin:
	git remote set-url origin https://nitrox.cerfacs.fr/open-source/flint.git

