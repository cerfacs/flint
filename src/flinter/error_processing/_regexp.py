"""Format analysis module.

This will use the regexp rules given in configuration file to identify errors.
It intends to follows the coding conventions
mentioned in
`OMS Documentation Wiki page <https://alm.engr.colostate.edu/cb/wiki/16983>`__

The replacement pipeline exists but is not implemented fully,
since I am not so sure of the result for multiple errors in one line.


Note:
    Algorithms implemented in this module take advantage of ordering to
    speedup lookups. Ensure `comments_spans` and `functions_info` are ordered
    accordingly.
"""

# TODO: replacement pipeline


def fmt_analysis(content, regexp_rules, comments_spans, functions_info):
    regexp_spans = _get_regexp_spans(content, regexp_rules)

    filtered_regexp_spans = _remove_if_within_comments(
        regexp_rules, regexp_spans, comments_spans)

    regexp_errors = _translate_span_info(content, filtered_regexp_spans)

    return _assign_errors_to_functions(regexp_errors, functions_info)


def _translate_span_info(content, regexp_spans):
    """From span to line number, col, span length, and line.
    """

    # get spans at beginning of each line
    lines = content.split('\n')
    line_spans = [0]
    for line in lines:
        line_spans.append(line_spans[-1] + len(line) + 1)

    # identify line and col
    regexp_errors = []
    line_span_gen = (k for k, _ in enumerate(line_spans))
    line_no = next(line_span_gen)
    for rule_name, span in regexp_spans:  # TODO: use filtered
        min_span, max_span = span
        span_length = max_span - min_span

        while min_span >= line_spans[line_no]:
            line_no = next(line_span_gen)

        span_line_no = line_no - 1
        col = min_span - line_spans[span_line_no]

        regexp_errors.append((rule_name, span_line_no + 1, col + 1, span_length, lines[span_line_no]))

    return regexp_errors


def _get_regexp_spans(content, regexp_rules):
    """Returns list of tuples (rule name and corresponding span).
    """
    regexp_spans = []
    for rule_name, rule in regexp_rules.items():
        for res in rule['regexp'].finditer(content):
            regexp_spans.append((rule_name, res.span()))

    regexp_spans.sort(key=lambda x: x[-1][0])

    return regexp_spans


def _is_within_comment(span, comment_span):
    if span[0] >= comment_span[0] and span[1] <= comment_span[1]:
        return True

    return False


def _remove_if_within_comments(regexp_rules, regexp_spans, comments_spans):
    """Removes false positives (i.e. errors found within comments).
    """
    # do nothing if not comments
    if len(comments_spans) == 0:
        return regexp_spans

    # remove comments
    comments_spans_gen = (comment_span for comment_span in comments_spans)
    comment_span = next(comments_spans_gen)
    filtered_regexp_spans = []
    for rule_name, regexp_span in regexp_spans:
        if regexp_rules[rule_name]['include-comments']:
            filtered_regexp_spans.append((rule_name, regexp_span))
            continue

        while regexp_span[0] > comment_span[1]:
            try:
                comment_span = next(comments_spans_gen)
            except StopIteration:
                break

        if not _is_within_comment(regexp_span, comment_span):
            filtered_regexp_spans.append((rule_name, regexp_span))

    return filtered_regexp_spans


def _assign_errors_to_functions(regexp_errors, functions_info):
    """Assigns global errors to corresponding functions.

    Notes:
        Assumes deeper nested levels appear first in `functions_info`.

        `functions_info` is a list of tuples: (function_name, (start_line,
         end_line).
    """
    errors = {}

    for function_name, (start_line, end_line) in functions_info:

        function_errors = []
        k = 0
        while len(regexp_errors) > 0:
            try:
                error_line = regexp_errors[k][1]
            except IndexError:
                break

            if error_line < start_line:
                k += 1
                continue

            elif error_line > end_line:
                break

            function_errors.append(regexp_errors.pop(k))

        errors[function_name] = _convert_errors_to_dict(function_errors)

    return errors


def _convert_errors_to_dict(regexp_errors):
    errors = {}
    for regexp_error in regexp_errors:
        name = regexp_error[0]
        if name in errors:
            error_list = errors[name]
        else:
            errors[name] = error_list = []

        error_list.append({
            'line_no': regexp_error[1],
            'column': regexp_error[2],
            'span': regexp_error[3],
            'line': regexp_error[4],
        })

    return errors
