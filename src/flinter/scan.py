
import os
from pathlib import Path

from flinter.nesting_utils import nest_data
from flinter.rules import get_default_rule_sets
from flinter.parser import get_all_analysable_files
from flinter.parser import parse_errors

# TODO: files to ignore (yaml?)]


def load_content(filename):
    try:
        with open(filename, 'r', encoding='utf-8') as file:
            content = file.read()

        return content

    except UnicodeDecodeError:
        print(f"File {filename} is not encoded in UTF-8")

    return None


def scan_dir(dirname, rule_sets=None, nest=True):
    if dirname == '.':
        dirname = f"..{os.sep}{Path('.').absolute().name}"

    if rule_sets is None:
        rule_sets = get_default_rule_sets()

    files_to_scan = get_all_analysable_files(dirname, rule_sets)
    tot = len(files_to_scan)
    raw_data_ls = []
    for i,filename in enumerate(files_to_scan):
        print(f"{i+1}/{tot} scanning {filename}")
        rules = rule_sets.get_from_fname(filename)
        info = scan_file(filename, rules, dirname=dirname)
        if info is not None:
            raw_data_ls.append(info)

    if nest:
        return nest_data(raw_data_ls)

    return raw_data_ls


def scan_file(filename, rules=None, dirname=None):

    if rules is None:
        rules = get_default_rule_sets().get_from_fname(filename)

    # load content
    content = load_content(filename)
    if content is None:  # unable to read file
        return None

    # get relative path
    if dirname is not None:
        dirpath = Path(dirname)
        filepath = Path(filename)
        path = filepath.relative_to(dirpath.parent).as_posix()
    else:
        path = filename

    return scan_content(path, content, rules)


def scan_content(path, content, rules):
    return parse_errors(path, content, rules)
