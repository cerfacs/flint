
import pkg_resources
import shutil

import yaml


# TODO: rethink location of this utils


MAP_DEFAULT_RULE_NAMES = {
    'fortran': 'fortran_rc_default',
    'python': 'python_rc_default',
    'cpp': 'cpp_rc_default'
}

DEFAULT_RULE_NAMES = tuple(MAP_DEFAULT_RULE_NAMES.values())


def _get_default_rule_filename(rule_name):
    return pkg_resources.resource_filename("flinter", f'default_rules/{rule_name}.yml')


def load_default_rule(rule_name):
    filename = _get_default_rule_filename(rule_name)
    return load_rule(filename)


def load_default_rules(default_rule_names=DEFAULT_RULE_NAMES):
    return {rule_name: load_default_rule(rule_name) for rule_name in default_rule_names}


def load_rule(filename):
    with open(filename) as fin:
        rules = yaml.load(fin, Loader=yaml.FullLoader)

    return rules


def copy_default_rule(language, filename=None):
    rule_name = MAP_DEFAULT_RULE_NAMES[language]

    if filename is None:
        filename = f'{rule_name}.yaml'

    shutil.copy2(_get_default_rule_filename(rule_name), filename)

    return filename


class NodePrinter:
    def __init__(self, title_getter, children_getter, arg_getter):
        self.title_getter = title_getter
        self.arg_getter = arg_getter
        self.children_getter = children_getter

    def _format(self, node, tab=''):
        dec = ' '
        result = ''
        args = list(self.arg_getter(node))
        children = [(self.title_getter(child), child) for child in self.children_getter(node)]
        lines = args + children
        for i, (title, arg) in enumerate(lines):
            result += '\n' + tab
            if i == len(lines) - 1:
                result += '└'
                pad = ' '
            else:
                result += '├'
                pad = '│'
            result += dec + title
            if i >= len(args):
                result += self._format(arg, tab + pad + dec)
            else:
                result += ': ' + str(arg)
        return result

    def __call__(self, node):
        print(self.title_getter(node) + self._format(node))
