"""
Flint
=====

Flint is a source-code static analyzer and quality checker for
fortran programming language.

The repostory is named Flinter, because Flint was a namespace already taken on PYPi.
"""


__version__ = "0.8.0"


# TODO: update default rules (most of info useless)
