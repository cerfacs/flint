"""Parser machinery.

Designed such that it will be trivial to extend to different parsers.
"""

import glob
import importlib


def _import_backend(backend='flizard'):
    return importlib.import_module(f'flinter.parser.{backend}')


def _config_backend(function):

    def wrapper(*args, **kwargs):
        # TODO: for now uses flizard only
        _backend = _import_backend(backend='flizard')
        func = function(*args, _backend=_backend, **kwargs)

        return func

    return wrapper


@_config_backend
def _get_available_exts(_backend=None):
    return _backend.get_available_exts()


def get_analysable_exts(rule_sets):
    parser_exts = _get_available_exts()
    return [ext for ext in parser_exts if rule_sets.is_ext_available(ext)]


def get_all_analysable_files(dirname, rule_sets):
    exts = get_analysable_exts(rule_sets)

    files = []
    for ext in exts:
        files.extend(glob.glob(fr'{dirname}/**/*.{ext}', recursive=True))

    return files


@_config_backend
def _parse_content(path, content, _backend=None, **kwargs):
    return _backend.parse_content(path, content, **kwargs)


def parse_content(path, content, **kwargs):
    return _parse_content(path, content, **kwargs)


@_config_backend
def _parse_errors(path, content, rules, _backend=None, **kwargs):
    return _backend.parse_errors(path, content, rules, **kwargs)


def parse_errors(path, content, rules, **kwargs):
    return _parse_errors(path, content, rules, **kwargs)
