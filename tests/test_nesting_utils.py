
import copy

import pytest

from flinter.nesting_utils import unnest_to_file_level
from flinter.nesting_utils import flatten
from flinter.nesting_utils import nest_data
from flinter.nesting_utils import get_subtree
from flinter.nesting_utils import remove_keys_from_nested

from .data import EXAMPLE_DICT


def _assert_keys_in_dict(keys, dict_):
    for key in keys:
        assert key in dict_


@pytest.mark.parametrize('fully', [True, False])
def test_unnest_to_file_level(fully):
    unnested_db = unnest_to_file_level(EXAMPLE_DICT, fully=fully)

    # test list result
    assert type(unnested_db) is list
    assert len(unnested_db) == 3

    expected_paths = ['root/file1.f90', 'root/folder2/file2.f90']
    for child, expected_path in zip(unnested_db, expected_paths):
        assert child['path'] == expected_path

    # look to aggregation ability
    file1 = unnested_db[0]
    regexp_rules = file1['regexp_rules']
    struct_rules = file1['struct_rules']

    _keys = ['shared', 'file', 'subroutine']
    regexp_keys = [f'regexp_{key}' for key in _keys]
    struct_keys = [f'struct_{key}' for key in _keys]

    if fully:
        _assert_keys_in_dict(regexp_keys, regexp_rules)
        assert len(regexp_rules['regexp_shared']) == 2

        _assert_keys_in_dict(struct_keys, struct_rules)
        for key in struct_keys:
            assert type(struct_rules[key]) is list
        assert len(struct_rules['struct_shared']) == 2
        assert len(struct_rules['struct_file']) == 1

    else:
        _assert_keys_in_dict(regexp_keys[:-1], regexp_rules)
        assert len(regexp_rules['regexp_shared']) == 1

        _assert_keys_in_dict(struct_keys[:-1], struct_rules)
        for key in struct_keys[:-1]:
            assert type(struct_rules[key]) is dict

    assert len(regexp_rules['regexp_file']) == 1


def test_nest_data():
    unnested_db = unnest_to_file_level(EXAMPLE_DICT, fully=False)
    nested_data = nest_data(unnested_db)

    assert len(nested_data['children']) == len(EXAMPLE_DICT['children'])

    # order is not kept in the operation
    initial_paths = [child['path'] for child in EXAMPLE_DICT['children']]
    for new_child in nested_data['children']:
        child = EXAMPLE_DICT['children'][initial_paths.index(new_child['path'])]
        assert len(new_child['children']) == len(child['children'])


@pytest.mark.parametrize('not_append_types', [(), ('subroutine',)])
@pytest.mark.parametrize('nested_db', [EXAMPLE_DICT, [EXAMPLE_DICT]])
def test_flatten(nested_db, not_append_types):
    unnested_db = flatten(nested_db, not_append_types)

    n = 6
    if len(not_append_types) == 0:
        assert len(unnested_db) == n
    else:
        assert len(unnested_db) == (n - 1)


def test_get_subtree():

    # check if finds the path
    path = 'root/folder2/file2.f90'
    subtree = get_subtree(EXAMPLE_DICT, path)

    target_dict = EXAMPLE_DICT['children'][1]['children'][0]
    for key in target_dict:
        assert key in subtree

    for key in subtree:
        assert subtree[key] == target_dict[key]

    # check it fails to find the path
    path = 'root/folder2/file2'
    subtree = get_subtree(EXAMPLE_DICT, path)
    assert subtree is False


def test_remove_keys_from_nested():
    key = 'struct_rules'
    example_dict = copy.deepcopy(EXAMPLE_DICT)
    remove_keys_from_nested(example_dict, [key, 'will_not_fail'])

    assert key not in example_dict
    assert key not in example_dict['children'][0]
    assert key not in example_dict['children'][1]
    assert key not in example_dict['children'][0]['children'][0]
