
import os

import pytest

from flinter.rules import init_languages_specs
from flinter.utils import load_default_rules
from flinter.utils import copy_default_rule
from flinter.utils import load_rule
from flinter.parser import get_analysable_exts
from flinter.parser import get_all_analysable_files
from flinter.parser._parser import _get_available_exts


# TODO: need to test accordingly to parser


# SUPPORTED_LANGS = {'c', 'cpp', 'f90', 'f', 'f70', 'py'}
SUPPORTED_LANGS = {'f90', 'f', 'f70', 'py'}


def test_load_default_rules():
    rules_dict = load_default_rules()

    target_keys = ['fortran', 'python', 'cpp']
    for key in target_keys:
        assert f'{key}_rc_default' in rules_dict


@pytest.mark.parametrize('provide_filename', [True, False])
def test_copy_load_rule(tmpdir, provide_filename):
    language = 'fortran'

    # copy
    if provide_filename:
        filename = os.path.join(tmpdir, 'my_fortran.yaml')
        copy_default_rule(language, filename)
    else:
        cur_dir = os.getcwd()
        os.chdir(tmpdir)
        filename = copy_default_rule(language)
        os.chdir(cur_dir)
        filename = os.path.join(tmpdir, filename)

    # load
    rule_dict = load_rule(filename)
    assert type(rule_dict) is dict


def test_get_available_exts():
    parser_exts = _get_available_exts()
    for lang in SUPPORTED_LANGS:
        assert lang in parser_exts


def test_get_analysable_exts():
    rule_sets = init_languages_specs()
    exts = get_analysable_exts(rule_sets)
    for lang in SUPPORTED_LANGS:
        assert lang in exts


def test_get_all_analysable_files(tmpdir):

    filenames = ['file.f90', 'file.py', 'folder/file.py', 'file.cpp',
                 'folder2/folder/file.c', 'file.txt']
    filenames = [os.path.join(tmpdir, filename) for filename in filenames]
    os.mkdir(os.path.join(tmpdir, 'folder'))
    os.mkdir(os.path.join(tmpdir, 'folder2'))
    os.mkdir(os.path.join(tmpdir, 'folder2', 'folder'))

    for filename in filenames:
        with open(filename, 'w'):
            pass

    rule_sets = init_languages_specs()
    analysable_filenames = get_all_analysable_files(tmpdir, rule_sets)

    for filename in filenames[:-3]:
        assert filename in analysable_filenames

    assert filenames[-1] not in analysable_filenames
