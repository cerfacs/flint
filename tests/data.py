

# a random dict with expected structure (struct_rules names are not valid)
# TODO: do proper struct_rules
EXAMPLE_DICT = {
    'type': 'folder',
    'name': 'root',
    'path': 'root',
    'size': 30,
    'struct_rules': {},
    'regexp_rules': {},
    'children': [
        {
            'type': 'file',
            'name': 'file1',
            'path': 'root/file1.f90',
            'size': 20,
            'struct_rules': {
                'struct_shared': {'num_lines': 20, 'max_allowed': 5},
                'struct_file': {'max_vals': 11, 'max_allowed': 4}
            },
            'regexp_rules': {
                'regexp_shared': [
                    {'line_no': 2, 'line': 'random', 'column': 9, 'span': 2}],
                'regexp_file': [
                    {'line_no': 3, 'line': 'another', 'column': 1, 'span': 4}
                ]
            },
            'children': [
                {
                    'type': 'subroutine',
                    'name': 'subroutine1',
                    'path': 'root/file1.f90/subroutine1',
                    'size': 10,
                    'struct_rules': {
                        'struct_shared': {'num_lines': 10, 'max_allowed': 5},
                        'struct_subroutine': {'num_vals': 4, 'max_allowed': 5},
                    },
                    'regexp_rules': {
                        'regexp_shared': [
                            {'line_no': 5, 'line': 'random', 'column': 3, 'span': 1}],
                        'regexp_subroutine': [
                            {'line_no': 6, 'line': 'child', 'column': 2, 'span': 4}]
                    },
                    'children': []
                },

            ],
        },
        {
            'type': 'folder',
            'name': 'folder2',
            'path': 'root/folder2',
            'size': 5,
            'struct_rules': {},
            'regexp_rules': {},
            'children': [
                {
                    'type': 'file',
                    'name': 'file2',
                    'path': 'root/folder2/file2.f90',
                    'size': 25,
                    'struct_rules': {},
                    'regexp_rules': {},
                    'children': []
                }
            ]
        },
        {
            'type': 'file',
            'name': 'file3',
            'path': 'root/file3.f90',
            'size': 5,
            'struct_rules': {},
            'regexp_rules': {},
            'children': []
        }
    ],
}
