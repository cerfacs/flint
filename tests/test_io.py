
import os

import yaml

from flinter.io import dump
from flinter.io import load


TEST_DICT = {'a': [1, 2]}


def _dump_yaml(filename, data):
    with open(filename, 'w') as yaml_out:
        yaml.dump(data, yaml_out)


def test_json(tmpdir):

    filename = os.path.join(tmpdir, 'test.json')

    # test dump
    dump(filename, TEST_DICT)

    # test load
    load(filename)


def test_yaml(tmpdir):

    filename1 = os.path.join(tmpdir, 'test.yaml')
    filename2 = os.path.join(tmpdir, 'test.yml')

    # dump (not available in flinter)
    _dump_yaml(filename1, TEST_DICT)
    _dump_yaml(filename2, TEST_DICT)

    # load (available for backwards compatibility)
    load(filename1)
    load(filename2)
