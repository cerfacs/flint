# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/).



## Unreleased

### Changed
- modified processors: in particular, comment spans replace the need of `clean_content` variable
- split between parsing and error computation: these tasks are now performed independently 
- improved errors computation (with new structure, e.g. `regexp` can be handled faster, as there's no need to go in a line by line manner)
- generalized error computations: inputs are parser-agnostic
- modified parser architecture to handle different parsers (even if for now only one is support)


### Fixed
- `fortran` and `python` parsers (wrapped `lizard` readers: several corrections; parsing is more robust


### Added
- `python` and `fortran` state machines to collect variable names



## [0.7.1] 2022 / 02 / 11

### Changed

- changed default rule for fortran to lowercase intrinsics
- added /example folder with two codes rules

### Fixed

- can now either enforce lowercase or uppercase intrinsics/types. use `regexp: \b({intrinsics_lower}|end(?:{blocks_lower}|{structs_lower}))\` for  lowercased list of intrinsics, and `{intrinsics_upper}` for the opposite.

## [0.7.0] 2022 / 02 / 09

### Added

- `--sort_by_line` to `lint`


## [0.6.2] 2022 / 01 / 24

### Added
* All Language words are also available in lower case to enable lower-case enforcement.
* Example of lower-case enforcement in the Fortran default rules 



## [0.6.1] 2021 / 12 / 17

### Fixed
* `-d` represents depth in `flint score` (database is represented by `-b`)



## [0.6.0] 2021 / 12 / 16

### Added
* CLI commands: `flint database`, `flint lint`, `flint stats`, `flint struct`


### Changed
* API has been thoroughly redesigned with little thought on backwards compatibility (most of the functionality has been kept and/or enhanced). More effort was put into the CLI API to ensure almost full backwards compatibility.
* small changes in already existing API commands (`flint score` and `flint config`)
* clear difference between raw data (database) and "derived data" (e.g. counts of number of errors)
* more information in `flint tree` visualization

### Removed
* `flint dump`: similar behavior achieved with `flint database` or `flint stats`


## [0.5.1] 2021 / 10 / 28

### Fixed
- `nobvisual`-related functions


## [0.5.0] 2021 / 08 / 12

### Added
- single file analysis
- multi-language support
- "extension", "namespace_blocks", "context_blocks" and "var-declaration" in rules files
- "case-sensitive" and "include-comments" in rule fields
- default values for rule fields
- default rules for Python and C/C++

### Changed
- structure analysis is delegated to Lizard
- format analysis supports case insensitive rules

### Fixed
 - structure analysis offset
 - default rules for Fortran

### Deprecated
- use of "blocks" in rules (new "namespace_blocks" and "context_blocks")
- use of "fortran-syntax" in rules (new "syntax")
- implicit "extension" and "var-declaration" (prefer explicit)


## [0.4.0] 2021 / 01 / 22

### Added
- customizable set of rules
- interactive visualization
- dump analysis of code in yml file

### Changed
- CLI still flint but with new arguments

### Fixed
- more resilient to old Fortran syntax
- cope with c and d comments markup

### Deprecated
- former CLI option `flint struc` and `flint pep8`


## [0.3.0] 2020 / 06 / 29
 
### Added
- Option all-files to run on recursives files

### Changed
- Score formula
- `cplx`mode becomes `pep8` mode.

 	
### Fixed
- Can skip files with non-utf8 characters.



## [0.2.3] 2020 / 06 / 25
 	
### Fixed
- stop pestering about doxygen documentation strings



## [0.2.2] 2020 / 02 / 27
 
### Fixed
 - add dependency to `click`
 - documentation fixed for read the docs

