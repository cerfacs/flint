.. _header-n0:

.. arnica documentation master file, created by
   sphinx-quickstart on Thu Dec  6 11:21:44 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



Welcome to Flint documentation!
===============================

.. toctree::
   :maxdepth: 2

   readme_link
   fortran_guidelines.md
   PEP008_like_rules
   changelog_link
