# PEP008-like rules

Some rules taken from PEP008 can be tested in FLINT.
The objective is to reduce **the reccurent cost of understanding the code** by :
- explicit naming.
- contexts (e.g. Function or subroutines) limited in size.

## Disclaimer

These rules are intended **to ease the pain of a reader at first sight, not the developper**. These will definitely be a hassle to implement for the initial developer.
But hey, if you are reading this, you expect some intrusive code quality guidelines dontcha?.

*As a rule of thumb you can assume that such rules will double the development time, halve the debugging time, and decrease tenfold the support time.*

## Rules in Flint

Rules presently enforced are:

### `too-many-levels`: Nested blocks

Flint will complain when, in the same context, there are too many levels of nested IF/DO blocks (*default 5 levels*).

### `too-many-arguments` : Large signatures

Complain when a fuction/subroutin signature call involves toomany elements (*default 5 arguments*).

### `line-too-long` : Long lines

Complain when a line is too long (*default 100 characters*). This becomes especially usefull when comparing/merging several versions of a code . For example, when you merge modifications, three versions of the code are displayed at the same time: "Mine", "Merge", and "Thiers". 

### `too-many-lines` : long contexts

Complain if a context becomes so big that the reader cannot remember the beginning of the context (*default 50 lines*). This forces the developper to create single-purpose pieces of code instead of a long and end-less script.

### `too-many-locals` : Large number of local variables

Complain if a context involves too many variables (*default 12 local variables*). When there are too many variables, the reader can hardly rememeber all of them at tfirst read.

### `invalid-name` : Bad naming habit

Complain if a variable is too short to be meaningfull (*default 3 characters*). You can consider that `u`is obviously a velocity, until you see people using it for a temperature in litterature. You can consider that `x`is obviously a coordinate, until you stumble on a *non-dimensionned coordinates*.





